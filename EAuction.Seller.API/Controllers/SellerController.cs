﻿using EAuction.Seller.API.Models;
using EAuction.Seller.API.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EAuction.Seller.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SellerController : ControllerBase
    {
        private readonly IProductService _productService;
        public SellerController(IProductService productService)
        {
            _productService = productService;
        }

        // GET: To get all products
        [HttpGet]
        [Route("get-product")]
        public IEnumerable<Product> GetProduct()
        {
            return _productService.GetProducts();
        }

        // GET product by id
        [HttpGet("{id}")]
        public string GetProduct(int id)
        {
            return "value";
        }

        // POST Add product detail
        [HttpPost]
        [Route("add-product")]
        public Product AddProduct([FromBody] Product model)
        {
           return _productService.AddProduct(model);
        }

        // POST To Add seller details 
        [HttpPost]
        [Route("add-seller")]
        public void AddSeller([FromBody] Models.Seller model)
        {
        }


        // DELETE To delete the product
        [HttpDelete("{id}")]
        [Route("delete-product/{productId}")]
        public void DeleteProduct(int productId)
        {
        }

        // To get the product category details
        [HttpGet]
        [Route("get-category")]
        public List<string> GetProductCategory()
        {
            return new List<string> { "Painting", "Sculptor", "Ornament" };
        }

        // PUT api/<SellerController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}
    }
}

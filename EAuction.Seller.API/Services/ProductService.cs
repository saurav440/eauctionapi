﻿using EAuction.Seller.API.Models;
using EAuction.Seller.API.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EAuction.Seller.API.Services
{
    public class ProductService : IProductService
    {
        private readonly ISellerRepository _sellerRepository;
        public ProductService(ISellerRepository sellerRepository)
        {
            _sellerRepository = sellerRepository;
        }
        public Product AddProduct(Product model)
        {
            return _sellerRepository.AddProduct(model);
        }

        public void DeleteProduct(string id)
        {
            throw new NotImplementedException();
        }

        public List<Product> GetProducts()
        {
            return _sellerRepository.GetProducts();
        }

       
    }
}

﻿using EAuction.Seller.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EAuction.Seller.API.Services
{
    public interface IProductService
    {
        Product AddProduct(Product value);

        List<Product> GetProducts();
        void DeleteProduct(string id);
    }
}

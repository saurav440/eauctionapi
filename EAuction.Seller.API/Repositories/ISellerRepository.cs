﻿using EAuction.Seller.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EAuction.Seller.API.Repositories
{
    public interface ISellerRepository
    {

        List<Product> GetProducts();
        Product AddProduct(Product model);
    }
}
